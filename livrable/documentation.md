# WikiCamp 

Suite a une demande de candidature pour un post de développeur, l'entreprise **WikiCampe**, 
Demander de réaliser un test technique en PhP symfony. Aanant de ce précipiter sur le codage il est primordiale, 
de réaliser un schéma avant tout, ainsi de réflechir pour la base de donnée, afin qu'on pusisse, respecter les exigences 
qu'on m'as fournie.


[Document](test_technique_symfony_2024.pdf)

## Schéma 

![App Screenshot](diagramme-entite-association.jpeg)

*diagramme d'entité associative*

![App Screenshot](mcd.jpeg)

*schéma MCD*


# Ressources

[Documentation Symfony 7](https://symfony.com/doc/current/index.html)

[Réalisation des diagrammes](https://lucid.app/lucidchart/8f3fa087-008a-41a7-997b-cb4f47984142/edit?viewport_loc=-1649%2C-857%2C4153%2C2079%2C0_0&invitationId=inv_f096bc15-1d53-485d-ad11-79411d75cb3d)

