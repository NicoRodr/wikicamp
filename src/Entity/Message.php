<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
class Message
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    public ?int $id_message = null;

    #[ORM\Column(length: 255)]
    public ?string $sender = null;

    #[ORM\Column(length: 255)]
    public ?string $recipient = null;

    #[ORM\Column(type: Types::DATE_IMMUTABLE)]
    public ?\DateTimeImmutable $date = null;

    #[ORM\Column(length: 255)]
    public ?string $object = null;

    #[ORM\Column(type: Types::TEXT)]
    public ?string $message = null;

    #[ORM\ManyToOne(inversedBy: 'id_message')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Member $id�_member = null;

    #[ORM\ManyToOne(inversedBy: 'id_message')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Annonces $id_annonce = null;

    public function getIdMessage(): ?int
    {
        return $this->id_message;
    }

    public function getSender(): ?string
    {
        return $this->sender;
    }

    public function setSender(string $sender): static
    {
        $this->sender = $sender;

        return $this;
    }

    public function getRecipient(): ?string
    {
        return $this->recipient;
    }

    public function setRecipient(string $recipient): static
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(string $object): static
    {
        $this->object = $object;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;

        return $this;
    }

    public function getId�Member(): ?Member
    {
        return $this->id�_member;
    }

    public function setId�Member(?Member $id�_member): static
    {
        $this->id�_member = $id�_member;

        return $this;
    }

    public function getIdAnnonce(): ?Annonces
    {
        return $this->id_annonce;
    }

    public function setIdAnnonce(?Annonces $id_annonce): static
    {
        $this->id_annonce = $id_annonce;

        return $this;
    }
}
