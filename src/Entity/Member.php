<?php

namespace App\Entity;

use App\Repository\MemberRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MemberRepository::class)]
class Member
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    public ?int $id_member = null;

    #[ORM\Column(length: 255)]
    public ?string $firstname = null;

    #[ORM\Column(length: 255)]
    public ?string $lastname = null;

    #[ORM\Column(length: 255)]
    public ?string $email = null;

    #[ORM\ManyToOne(inversedBy: 'id_member')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Role $id_role = null;

    #[ORM\OneToMany(targetEntity: Notification::class, mappedBy: 'id_member')]
    private Collection $id_notification;

    #[ORM\OneToMany(targetEntity: Message::class, mappedBy: 'id�_member', orphanRemoval: true)]
    private Collection $id_message;

    public function __construct()
    {
        $this->id_notification = new ArrayCollection();
        $this->id_message = new ArrayCollection();
    }

    public function getIdMember(): ?int
    {
        return $this->id_member;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function setIdMember(int $id_member): static
    {
        $this->id_member = $id_member;

        return $this;
    }

    public function getIdRole(): ?Role
    {
        return $this->id_role;
    }

    public function setIdRole(?Role $id_role): static
    {
        $this->id_role = $id_role;

        return $this;
    }

    /**
     * @return Collection<int, Notification>
     */
    public function getIdNotification(): Collection
    {
        return $this->id_notification;
    }

    public function addIdNotification(Notification $idNotification): static
    {
        if (!$this->id_notification->contains($idNotification)) {
            $this->id_notification->add($idNotification);
            $idNotification->setIdMember($this);
        }

        return $this;
    }

    public function removeIdNotification(Notification $idNotification): static
    {
        if ($this->id_notification->removeElement($idNotification)) {
            // set the owning side to null (unless already changed)
            if ($idNotification->getIdMember() === $this) {
                $idNotification->setIdMember(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getIdMessage(): Collection
    {
        return $this->id_message;
    }

    public function addIdMessage(Message $idMessage): static
    {
        if (!$this->id_message->contains($idMessage)) {
            $this->id_message->add($idMessage);
            $idMessage->setId�Member($this);
        }

        return $this;
    }

    public function removeIdMessage(Message $idMessage): static
    {
        if ($this->id_message->removeElement($idMessage)) {
            // set the owning side to null (unless already changed)
            if ($idMessage->getId�Member() === $this) {
                $idMessage->setId�Member(null);
            }
        }

        return $this;
    }
}
