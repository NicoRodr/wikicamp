<?php

namespace App\Entity;

use App\Repository\RoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RoleRepository::class)]
class Role
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    public ?int $id_role = null;

    #[ORM\Column]
    public ?bool $profil = null;

    #[ORM\Column(length: 255)]
    public ?string $name = null;

    #[ORM\OneToMany(targetEntity: Member::class, mappedBy: 'id_role', orphanRemoval: true)]
    private Collection $id_member;

    public function __construct()
    {
        $this->id_member = new ArrayCollection();
    }

    public function getIdRole(): ?int
    {
        return $this->id_role;
    }

    public function isProfil(): ?bool
    {
        return $this->profil;
    }

    public function setProfil(bool $profil): static
    {
        $this->profil = $profil;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Member>
     */
    public function getIdMember(): Collection
    {
        return $this->id_member;
    }

    public function addIdMember(Member $idMember): static
    {
        if (!$this->id_member->contains($idMember)) {
            $this->id_member->add($idMember);
            $idMember->setIdRole($this);
        }

        return $this;
    }

    public function removeIdMember(Member $idMember): static
    {
        if ($this->id_member->removeElement($idMember)) {
            // set the owning side to null (unless already changed)
            if ($idMember->getIdRole() === $this) {
                $idMember->setIdRole(null);
            }
        }

        return $this;
    }
}
