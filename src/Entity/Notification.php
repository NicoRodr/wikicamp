<?php

namespace App\Entity;

use App\Repository\NotificationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: NotificationRepository::class)]
class Notification
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    public ?int $id_notification = null;

    #[ORM\Column(length: 255)]
    public ?string $title = null;

    #[ORM\Column(length: 255)]
    public ?string $type = null;

    #[ORM\Column(type: Types::TEXT)]
    public ?string $message = null;

    #[ORM\ManyToOne(inversedBy: 'id_notification')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Member $id_member = null;

    public function getIdNotification(): ?int
    {
        return $this->id_notification;
    }

    public function setIdNotification(int $id_notification): static
    {
        $this->id_notification = $id_notification;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;

        return $this;
    }

    public function getIdMember(): ?Member
    {
        return $this->id_member;
    }

    public function setIdMember(?Member $id_member): static
    {
        $this->id_member = $id_member;

        return $this;
    }
}
