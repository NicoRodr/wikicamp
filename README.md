![Logo](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/th5xamgrr6se0x5ro4g6.png)


# wikicamp
A brief description of what this project does and who it's for

## Technical instructions
- Base de données : Utiliser Doctrine ORM pour la modélisation et la gestion de la base de données. Concevoir un schéma de base de données adapté aux besoins de l'application.
- Frontend : Utiliser Twig pour les templates. Assurer que l'interface est propre et utilisable.
- Sécurité : Appliquer les meilleures pratiques de sécurité pour protéger les données des utilisateurs et l'accès non autorisé aux annonces.
* Bonus (optionnel) : Ajouter une fonctionnalité de filtrage avancé pour la recherche d'annonces (par exemple, filtrer par disponibilité, équipements, taille du camping-car).


## Roadmap

- Additional browser support

- Add more integrations

## Tech Stack use

**Client :** Twig, Synfony

**Server :** MySQL

**Langage :** PhP

**Data Bases :** Doctrime ORM

**Schema :** MCD, MPD


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```shell
cd existing_repo
git remote add origin https://gitlab.com/NicoRodr/wikicamp.git
git branch -M main
git push -uf origin main
```



## Screenshots

![App Screenshot](https://via.placeholder.com/468x300?text=App+Screenshot+Here)


## Demo

Insert gif or link to demo


## Documentation

[Documentation](https://linktodocumentation)


## Acknowledgements

 - [Awesome Readme Templates](https://awesomeopensource.com/project/elangosundar/awesome-README-templates)
 - [Awesome README](https://github.com/matiassingers/awesome-readme)
 - [How to write a Good readme](https://bulldogjob.com/news/449-how-to-write-a-good-readme-for-your-github-project)


## Appendix

Any additional information goes here

## 🚀 About Me
I'm a full stack developer...


## 🛠 Skills
### Developpement
### Gestion project
### DevOps



## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://katherineoelsner.com/)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/)
[![twitter](https://img.shields.io/badge/twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/)

## Authors

- [@NicoRodr ](https://gitlab.com/NicoRodr)